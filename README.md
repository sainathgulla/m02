M02 - VS Code, Repos

Install Visual Studio Code (https://code.visualstudio.com/c?utm_expid=101350005-28.R1T8FshdTBWEfZjY0s7XKQ.2), a light-weight Interactive Development Environment (IDE).

Sign up for a BitBucket (https://bitbucket.org/) code hosting account with your school email. 

Sign up for a GitHub (https://github.com/) code hosting account with your school email. 